Welcome to MABM_Academic Documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 3

   intro
   abmhack
   mabm
   matsim




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
