.. _abmhack:

ABM Hack
============
ABM Hack is an interdisciplinary and cross-institutional hackathon for large-scale urban simulation to be held at RMIT University on 1st February 2019.The session aims to draw on the collective knowledge of participants to build models of cycling behaviour into the Melbourne Activity Based Model (MABM).

We’re delighted to announce that Infrastructure Victoria have agreed to sponsor the event and will be providing us access to MABM. MABM enables us to simulate the activities and travel of every person in Melbourne. MABM is created using MATSim framework and Victorian Integrated Survey of Travel and Activity 2016 (VISTA16) data. Using the simulation, we can ask questions of this synthetic population that are difficult and/or costly to ask of the actual population.

Our focus for the event will be on active transport, focussing on cycling. Our plan is to tap into the collective knowledge of hackathon participants to work out how we can build realistic models of cycling behaviour into MABM. This then enables us to plan for and test the effects of particular interventions. To start with we’re investigating the effects of one of Victoria’s biggest new cycling infrastructure projects being built as part of the West Gate Tunnel Project.
